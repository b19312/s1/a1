package com.zuitt.batch193;

import java.util.Scanner;

public class Activity {

    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = appScanner.nextLine().trim();

        System.out.println("Last Name:");
        String lastName = appScanner.nextLine().trim();

        System.out.println("First Subject Grade:");
        double firstGrade = appScanner.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondGrade = appScanner.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdGrade = appScanner.nextDouble();


        double total = (double) ((firstGrade + secondGrade + thirdGrade)/4);
        System.out.println("Good day, " + firstName + " " + lastName + ". Your grade average is: " + total);


    }
}
